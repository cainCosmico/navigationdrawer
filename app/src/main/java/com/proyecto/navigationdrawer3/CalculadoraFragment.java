package com.proyecto.navigationdrawer3;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class CalculadoraFragment extends Fragment {

    // Widgets de interfaz
    private TextView panel;
    private TextView result;
    private String query;
    private View view;

    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");

    DataPassListener mCallback;
    private Context myContext;
    public interface DataPassListener{
        public void passData(String data);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        // This makes sure that the host activity has implemented the callback interface
        // If not, it throws an exception
        try
        {
            mCallback = (DataPassListener) activity;
            System.out.println("CALLBACK: " + mCallback);
//            myContext = activity;
        }
        catch (ClassCastException e)
        {
            System.out.println("Error en set mCallback");
        }
    }

    public static CalculadoraFragment newInstance() {
        return new CalculadoraFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.calculadora_fragment, container, false);

        query = "";

        panel = view.findViewById(R.id.textView);
        panel.setText("");

        result = view.findViewById(R.id.textViewResult);
        result.setText("");

        // Event Listener
        initOnClickListeners();

        return view;
    }

    public void initOnClickListeners() {
        // Calcula el resultado al textear
        panel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) buttonIgualAction();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Evento de buttons
        Button btnCE = view.findViewById(R.id.button10);
        btnCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCEAction();
            }
        });

        Button btnDel = view.findViewById(R.id.button);
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonBorrarAction();
            }
        });

        Button btnIgual = view.findViewById(R.id.button_igual);
        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonIgualAction();
            }
        });

        // Texto
        View.OnClickListener listenerSetText = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v.findViewById(v.getId());
                setTextView(b.getText().toString());
            }
        };

        Button btn0 = view.findViewById(R.id.button0);
        Button btn1 = view.findViewById(R.id.button1);
        Button btn2 = view.findViewById(R.id.button2);
        Button btn3 = view.findViewById(R.id.button3);
        Button btn4 = view.findViewById(R.id.button4);
        Button btn5 = view.findViewById(R.id.button5);
        Button btn6 = view.findViewById(R.id.button6);
        Button btn7 = view.findViewById(R.id.button7);
        Button btn8 = view.findViewById(R.id.button8);
        Button btn9 = view.findViewById(R.id.button9);
        Button btnPunto = view.findViewById(R.id.button_punto);

        Button btnMas    = view.findViewById(R.id.button_mas);
        Button btnMenos  = view.findViewById(R.id.button_res);
        Button btnPor    = view.findViewById(R.id.button_mul);
        Button btnDivido = view.findViewById(R.id.button_div);

        btn0.setOnClickListener(listenerSetText);
        btn1.setOnClickListener(listenerSetText);
        btn2.setOnClickListener(listenerSetText);
        btn3.setOnClickListener(listenerSetText);
        btn4.setOnClickListener(listenerSetText);
        btn5.setOnClickListener(listenerSetText);
        btn6.setOnClickListener(listenerSetText);
        btn7.setOnClickListener(listenerSetText);
        btn8.setOnClickListener(listenerSetText);
        btn9.setOnClickListener(listenerSetText);

        btnPunto.setOnClickListener(listenerSetText);
        btnMas.setOnClickListener(listenerSetText);
        btnMenos.setOnClickListener(listenerSetText);
        btnPor.setOnClickListener(listenerSetText);
        btnDivido.setOnClickListener(listenerSetText);
    }

    // text view
    public void setTextView(String c){
        panel.append(c);
    }

    // Este metodo realiza la operacion con Javascript
    public String calculateResult(String cadena) {
        // Crea Engine Javascript
        try {
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("js");
            Double auxResult = (double) engine.eval(cadena);
            cadena = REAL_FORMATTER.format(auxResult);

            System.out.println("result  = " + auxResult);
        } catch (ScriptException e) {
//            String aux = panel.getText().toString();
//            aux = aux.substring(0,aux.length()-1);
//            panel.setText(aux);
            cadena = getString(R.string.syserror);

            e.printStackTrace();
        }

        return cadena;
    }

    // @ button event
    public void buttonIgualAction() {
        query = panel.getText().toString();
        query = this.calculateResult(query);
        result.setText(query);

        // Pasar String a historia fragment
        ((MainActivity)getActivity()).passData(query);
//        if (mCallback != null) mCallback.passData(query);

    }

    // @ button event CE
    public void buttonCEAction() {
        this.panel.setText("");
    }

    // @ button event <-
    public void buttonBorrarAction() {
        if (panel.getText().length() > 1) {
            String aux = this.panel.getText().toString();
            aux = aux.substring(0,aux.length()-1);
            this.panel.setText(aux);
        } else {

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
//        public void onAboutFragmentInteraction(String string);
    }
}
