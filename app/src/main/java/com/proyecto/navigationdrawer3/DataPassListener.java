package com.proyecto.navigationdrawer3;

public interface DataPassListener {
    public void passData(String data);
}
