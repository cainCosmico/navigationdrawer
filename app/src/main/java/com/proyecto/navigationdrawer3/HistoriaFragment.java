package com.proyecto.navigationdrawer3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HistoriaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HistoriaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoriaFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final static String DATA_RECEIVE = "data_receive";
    // Owns
    private FragmentActivity myContext;
    private View view;
    private EditText editText;
    private TextView textViewResponse;
    private Button button;

    private OnFragmentInteractionListener mListener;

    public HistoriaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HistoriaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoriaFragment newInstance(String param1, String param2) {
        HistoriaFragment fragment = new HistoriaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void ejecutar() {
        SharedPreferences prefs = this.myContext.getSharedPreferences("datos",Context.MODE_PRIVATE);
        Editor editor=prefs.edit();
        editor.putString("mail", editText.getText().toString());
        editor.commit();
    }

    public void grabar() {
        //file = new File(Environment.getExternalStorageDirectory(),"MyResearchFile.txt");
        //            FileOutputStream fos = new FileOutputStream(file);
        try {
            OutputStreamWriter archivo = new OutputStreamWriter(this.myContext.openFileOutput(
                    "notas.txt", Context.MODE_PRIVATE));
            archivo.write( textViewResponse.getText() + "\n" + editText.getText().toString());
            archivo.flush();
            archivo.close();
        } catch (IOException e) {
        }
        Toast t = Toast.makeText(this.myContext, "Los datos fueron grabados",
                Toast.LENGTH_SHORT);
        t.show();
        updateTextView();
    }

    private boolean existe(String[] archivos, String archbusca) {
        for (int f = 0; f < archivos.length; f++)
            if (archbusca.equals(archivos[f]))
                return true;
        return false;
    }

    public void updateTextView() {
        String[] archivos = this.myContext.fileList();
        if (existe(archivos, "notas.txt")) {
            try {
                InputStreamReader archivo = new InputStreamReader(this.myContext.openFileInput("notas.txt"));
                BufferedReader br = new BufferedReader(archivo);
                String linea = br.readLine();
                String todo = "";
                while (linea != null) {
                    todo = todo + linea + "\n";
                    linea = br.readLine();
                    //System.out.println(todo);
                }
                br.close();
                archivo.close();
                textViewResponse.setText(todo);

            } catch (IOException e) {
            }
        }
    }

//    protected void displayReceivedData(String message) {
//        textViewResponse.append("Data received: "+message);
//        System.out.println("RESULTADO: " + message);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_historia, container, false);

        // Variables
        editText = view.findViewById(R.id.editText);
        textViewResponse = this.view.findViewById(R.id.textViewResponse);
        button = view.findViewById(R.id.button);

        //listeners
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grabar();
            }
        });

        updateTextView(); // Actualiza notas.txt en fragmet

        //Intent
        Bundle args = getArguments();
        if (args != null) {
            String message = args.getString("message");
            System.out.println("*****************************************************************");
            System.out.println("RESULTADO: " + message);
        }


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        myContext=(FragmentActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
